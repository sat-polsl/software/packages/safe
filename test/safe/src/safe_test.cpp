#include <span>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "etl/crc32.h"
#include "safe/safe.h"
#include "satext/struct.h"

namespace {

using namespace satext::struct_literals;
using namespace ::testing;

struct test_type {
    std::uint32_t a;
    std::uint32_t b;
};

class serializer {
public:
    static void serialize(const test_type& value, std::span<std::byte> buffer) {
        satext::pack_to("<II"_fmt, buffer, value.a, value.b);
    }

    static void deserialize(std::span<const std::byte> buffer, test_type& value) {
        satext::unpack("<II"_fmt, buffer).map([&value](auto unpacked) {
            auto [a, b] = unpacked;
            value.a = a;
            value.b = b;
        });
    }
};

class storage {
public:
    explicit storage(std::span<std::byte> buffer) : buffer_{buffer} {}

    bool read(std::size_t offset, std::span<std::byte> buffer) const {
        if (offset + buffer.size() > buffer_.size()) {
            return false;
        }

        std::copy(
            buffer_.begin() + offset, buffer_.begin() + offset + buffer.size(), buffer.begin());
        return true;
    }

    bool write(std::size_t offset, std::span<const std::byte> buffer) const {
        if (offset + buffer.size() > buffer_.size()) {
            return false;
        }

        std::ranges::copy(buffer, buffer_.begin() + offset);

        return true;
    }

private:
    std::span<std::byte> buffer_;
};

class crc {
public:
    std::uint32_t operator()(std::span<const std::byte> buffer) const {
        etl::crc32 crc;
        for (auto&& b : buffer) {
            crc.add(static_cast<std::uint8_t>(b));
        }
        return crc.value();
    }
};

TEST(SafeTest, InitializeWithEmptyStorage) {
    std::array<std::byte, 30> buffer{};
    storage s{buffer};
    safe::safe<test_type, serializer, storage> safe(s, crc{});

    safe.initialize();

    auto v = safe.get();
    ASSERT_THAT(v.a, Eq(0));
    ASSERT_THAT(v.b, Eq(0));
    ASSERT_THAT(safe.get_current_slot(), 1);
}

TEST(SafeTest, InitializeWithValidFirstSlot) {
    std::array<std::byte, 30> buffer{std::byte{},
                                     std::byte{0xef},
                                     std::byte{0xbe},
                                     std::byte{0xad},
                                     std::byte{0xde},
                                     std::byte{0xde},
                                     std::byte{0xc0},
                                     std::byte{0xed},
                                     std::byte{0xfe},
                                     std::byte{0x01},
                                     std::byte{0x15},
                                     std::byte{0x2e},
                                     std::byte{0x3a}};
    storage s{buffer};
    safe::safe<test_type, serializer, storage> safe(s, crc{});

    safe.initialize();

    auto v = safe.get();
    ASSERT_THAT(v.a, Eq(0xdeadbeef));
    ASSERT_THAT(v.b, Eq(0xfeedc0de));
    ASSERT_THAT(safe.get_current_slot(), 0);
}

TEST(SafeTest, InitializeWithValidSecondSlot) {
    std::array<std::byte, 30> buffer{
        std::byte{1},    std::byte{},     std::byte{},     std::byte{},     std::byte{},
        std::byte{},     std::byte{},     std::byte{},     std::byte{},     std::byte{},
        std::byte{},     std::byte{},     std::byte{},     std::byte{0xef}, std::byte{0xbe},
        std::byte{0xad}, std::byte{0xde}, std::byte{0xde}, std::byte{0xc0}, std::byte{0xed},
        std::byte{0xfe}, std::byte{0x01}, std::byte{0x15}, std::byte{0x2e}, std::byte{0x3a}};
    storage s{buffer};
    safe::safe<test_type, serializer, storage> safe(s, crc{});

    safe.initialize();

    auto v = safe.get();
    ASSERT_THAT(v.a, Eq(0xdeadbeef));
    ASSERT_THAT(v.b, Eq(0xfeedc0de));
    ASSERT_THAT(safe.get_current_slot(), 1);
}

TEST(SafeTest, InitializeWithValidSecondSlotAndInvalidFirstSlot) {
    std::array<std::byte, 30> buffer{
        std::byte{0},    std::byte{},     std::byte{},     std::byte{},     std::byte{},
        std::byte{},     std::byte{},     std::byte{},     std::byte{},     std::byte{},
        std::byte{},     std::byte{},     std::byte{},     std::byte{0xef}, std::byte{0xbe},
        std::byte{0xad}, std::byte{0xde}, std::byte{0xde}, std::byte{0xc0}, std::byte{0xed},
        std::byte{0xfe}, std::byte{0x01}, std::byte{0x15}, std::byte{0x2e}, std::byte{0x3a}};
    storage s{buffer};
    safe::safe<test_type, serializer, storage> safe(s, crc{});

    safe.initialize();

    auto v = safe.get();
    ASSERT_THAT(v.a, Eq(0xdeadbeef));
    ASSERT_THAT(v.b, Eq(0xfeedc0de));
    ASSERT_THAT(safe.get_current_slot(), 1);
}

TEST(SafeTest, InitializeWithInvalidSlotNumber) {
    std::array<std::byte, 30> buffer{std::byte{2}};
    storage s{buffer};
    safe::safe<test_type, serializer, storage> safe(s, crc{});

    safe.initialize();
    ASSERT_THAT(safe.get_current_slot(), 1);
    ASSERT_THAT(buffer[0], Eq(std::byte{1}));
    ASSERT_THAT(buffer[13], Eq(std::byte{}));
    ASSERT_THAT(buffer[14], Eq(std::byte{}));
    ASSERT_THAT(buffer[15], Eq(std::byte{}));
    ASSERT_THAT(buffer[16], Eq(std::byte{}));
    ASSERT_THAT(buffer[17], Eq(std::byte{}));
    ASSERT_THAT(buffer[18], Eq(std::byte{}));
    ASSERT_THAT(buffer[19], Eq(std::byte{}));
    ASSERT_THAT(buffer[20], Eq(std::byte{}));
    ASSERT_THAT(buffer[21], Eq(std::byte{0x69}));
    ASSERT_THAT(buffer[22], Eq(std::byte{0xdf}));
    ASSERT_THAT(buffer[23], Eq(std::byte{0x22}));
    ASSERT_THAT(buffer[24], Eq(std::byte{0x65}));
}

TEST(SafeTest, SetGetValue) {
    std::array<std::byte, 30> buffer{};
    storage s{buffer};
    safe::safe<test_type, serializer, storage> safe(s, crc{});

    safe.initialize();
    safe.set({0xdeadbeef, 0xfeedc0de});
    ASSERT_THAT(safe.get_current_slot(), Eq(0));
    ASSERT_THAT(buffer[0], Eq(std::byte{0}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0xef}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0xbe}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[4], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[5], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[6], Eq(std::byte{0xc0}));
    ASSERT_THAT(buffer[7], Eq(std::byte{0xed}));
    ASSERT_THAT(buffer[8], Eq(std::byte{0xfe}));
    ASSERT_THAT(buffer[9], Eq(std::byte{0x01}));
    ASSERT_THAT(buffer[10], Eq(std::byte{0x15}));
    ASSERT_THAT(buffer[11], Eq(std::byte{0x2e}));
    ASSERT_THAT(buffer[12], Eq(std::byte{0x3a}));

    auto v = safe.get();
    ASSERT_THAT(v.a, Eq(0xdeadbeef));
    ASSERT_THAT(v.b, Eq(0xfeedc0de));
}

TEST(SafeTest, SetGetProjection) {
    std::array<std::byte, 30> buffer{};
    storage s{buffer};
    safe::safe<test_type, serializer, storage> safe(s, crc{});

    safe.initialize();
    safe.set(&test_type::b, 0xdeadbeef);
    ASSERT_THAT(safe.get_current_slot(), Eq(0));
    ASSERT_THAT(buffer[0], Eq(std::byte{0}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0}));
    ASSERT_THAT(buffer[4], Eq(std::byte{0}));
    ASSERT_THAT(buffer[5], Eq(std::byte{0xef}));
    ASSERT_THAT(buffer[6], Eq(std::byte{0xbe}));
    ASSERT_THAT(buffer[7], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[8], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[9], Eq(std::byte{0x6a}));
    ASSERT_THAT(buffer[10], Eq(std::byte{0x60}));
    ASSERT_THAT(buffer[11], Eq(std::byte{0x3c}));
    ASSERT_THAT(buffer[12], Eq(std::byte{0x5e}));

    ASSERT_THAT(safe.get(&test_type::b), Eq(0xdeadbeef));
}

TEST(SafeTest, MultipleSets) {
    std::array<std::byte, 30> buffer{};
    storage s{buffer};
    safe::safe<test_type, serializer, storage> safe(s, crc{});

    safe.initialize();
    safe.set(&test_type::b, 0xdeadbeef);
    ASSERT_THAT(safe.get_current_slot(), Eq(0));
    ASSERT_THAT(buffer[0], Eq(std::byte{0}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0}));
    ASSERT_THAT(buffer[4], Eq(std::byte{0}));
    ASSERT_THAT(buffer[5], Eq(std::byte{0xef}));
    ASSERT_THAT(buffer[6], Eq(std::byte{0xbe}));
    ASSERT_THAT(buffer[7], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[8], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[9], Eq(std::byte{0x6a}));
    ASSERT_THAT(buffer[10], Eq(std::byte{0x60}));
    ASSERT_THAT(buffer[11], Eq(std::byte{0x3c}));
    ASSERT_THAT(buffer[12], Eq(std::byte{0x5e}));

    ASSERT_THAT(safe.get(&test_type::b), Eq(0xdeadbeef));

    safe.set(&test_type::a, 0xfeedc0de);
    ASSERT_THAT(safe.get_current_slot(), Eq(1));
    ASSERT_THAT(buffer[0], Eq(std::byte{1}));
    ASSERT_THAT(buffer[13], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[14], Eq(std::byte{0xc0}));
    ASSERT_THAT(buffer[15], Eq(std::byte{0xed}));
    ASSERT_THAT(buffer[16], Eq(std::byte{0xfe}));
    ASSERT_THAT(buffer[17], Eq(std::byte{0xef}));
    ASSERT_THAT(buffer[18], Eq(std::byte{0xbe}));
    ASSERT_THAT(buffer[19], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[20], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[21], Eq(std::byte{0xaf}));
    ASSERT_THAT(buffer[22], Eq(std::byte{0x7d}));
    ASSERT_THAT(buffer[23], Eq(std::byte{0x68}));
    ASSERT_THAT(buffer[24], Eq(std::byte{0xdc}));

    safe.set(&test_type::b, 0xdeadc0de);
    ASSERT_THAT(safe.get_current_slot(), Eq(0));
    ASSERT_THAT(buffer[0], Eq(std::byte{0}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0xc0}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0xed}));
    ASSERT_THAT(buffer[4], Eq(std::byte{0xfe}));
    ASSERT_THAT(buffer[5], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[6], Eq(std::byte{0xc0}));
    ASSERT_THAT(buffer[7], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[8], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[9], Eq(std::byte{0x31}));
    ASSERT_THAT(buffer[10], Eq(std::byte{0xa7}));
    ASSERT_THAT(buffer[11], Eq(std::byte{0x90}));
    ASSERT_THAT(buffer[12], Eq(std::byte{0xca}));
}

} // namespace

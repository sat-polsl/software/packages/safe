#include <cstdint>
#include <span>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "safe/serialized_sizeof.h"

namespace {

using namespace ::testing;

struct test_type {
    std::uint32_t a;
    std::uint32_t b;
};

class serializer_without_size {
public:
    static void serialize(const test_type&, std::span<std::byte>) {}
    static void deserialize(std::span<const std::byte>, test_type&) {}
};

class serializer_with_size {
public:
    static constexpr std::size_t size = 16;
    static void serialize(const test_type&, std::span<std::byte>) {}
    static void deserialize(std::span<const std::byte>, test_type&) {}
};

TEST(SerializedSizeTest, SerializerWithoutSize) {
    auto result = safe::serialized_sizeof<test_type, serializer_without_size>();
    ASSERT_THAT(result, Eq(8));
}

TEST(SerializedSizeTest, SerializerWithSize) {
    auto result = safe::serialized_sizeof<test_type, serializer_with_size>();
    ASSERT_THAT(result, Eq(16));
}

} // namespace

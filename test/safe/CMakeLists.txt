set(TARGET safe_test)

add_executable(${TARGET})

target_sources(${TARGET} PRIVATE
    src/safe_test.cpp
    src/serialized_size_test.cpp
    )

target_link_libraries(${TARGET}
    PRIVATE
    safe
    etl::etl
    )

add_unit_test(${TARGET})

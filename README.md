[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_safe&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sat-polsl_safe)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_safe&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sat-polsl_safe)
[![Pipeline](https://gitlab.com/sat-polsl/software/packages/safe/badges/main/pipeline.svg)](https://gitlab.com/sat-polsl/software/packages/safe/-/pipelines?page=1&scope=all&ref=main)
[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F46436002%2Frepository%2Ftags)](https://gitlab.com/sat-polsl/software/packages/safe/-/tags)

# Safe

## Overview

This package provides class to handle safe and reliable storeg in memory (internal or external).

### Rules

1. Use conventional commits.
2. Update tag with version after merging to `main`.
3. Use `snake_case` for everything except template arguments which are named with `CamelCase`

## Usage

1. Define storage class. Storage class shall implement two methods:
    - `bool write(std::size_t offset, std::span<const std::byte> data)` - writes data to storage at given offset and returns true if operation was successful.
    - `bool read(std::size_t offset, std::span<std::byte> data)` - reads data from storage at given offset and returns true if operation was successful.

2. Define serializer class for type `T`. Serializer class shall implement two static methods:
    - `void serialize(const T& object, std::span<std::byte> buffer)` - serializes object to data buffer.
    - `void deserialize(std::span<const std::byte> data, T& object)` - deserializes object from data buffer.

3. Define crc functor. Crc functor shall implement.
    - `std::uint32_t operator()(std::span<const std::byte> data)` - calculates crc of data.

4. Define, initialize safe storage and use `get()` and `set()` methods to access data:

```cpp

class my_type
{
  std::uint32_t u;  
  float f;
};

safe::safe<T, serializer, storage> safe_storage(storage, crc);
safe_storage.initializer();

auto t = safe_storage.get();

t.u = 1;
t.f = 2.0f;

safe_storage.set(t);

safe_storage.set(&my_type::u, 3);
safe_storage.set(&my_type::f, 4.0f);
```

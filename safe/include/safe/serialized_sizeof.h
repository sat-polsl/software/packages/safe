#pragma once
#include <concepts>
#include "safe/concepts.h"

namespace safe {

namespace detail {

template<typename T>
concept has_size = requires(T t) { T::size; };

}

/**
 * @brief Compile-time function that returns serialized size of provided Serializer
 * or if Serializer does not have size field it returns size of T.
 * @tparam T Serializable type.
 * @tparam Serializer Serializer type.
 * @return `Serializer::size` or `sizeof(T)`.
 */
template<typename T, serializer<T> Serializer>
consteval std::size_t serialized_sizeof() {
    if constexpr (detail::has_size<Serializer>) {
        return Serializer::size;
    } else {
        return sizeof(T);
    }
}
} // namespace safe

#pragma once
#include <array>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <span>
#include "safe/concepts.h"
#include "safe/serialized_sizeof.h"
#include "satext/expected.h"
#include "satext/inplace_function.h"
#include "satext/struct.h"

namespace safe {

/**
 * @ingroup safe
 * @{
 */

/**
 * @brief Class that provides safe storage in random access memory (internal or external) for any
 * type. Type is serialized and deserialized using provided serializer. Storage is divided into two
 * slots. Slots are identified by status byte. CRC is used to verify data integrity. Upon
 * initialization, safe will try to load data from slot identified by status byte. If slot is not
 * valid, it will try to load data from other slot. If both slots are invalid, safe will initialize
 * data with default value.
 *
 * Value can be retrieved using get() method, and then saved using set() method.
 * Both get() and set() are overloaded with Projection argument to avoid unnecessary copies.
 *
 * @tparam T Value type.
 * @tparam Serializer Serializer type.
 * @tparam Storage Storage type.
 */
template<typename T, serializer<T> Serializer, storage Storage>
class safe {
public:
    using crc_callback = satext::inplace_function<std::uint32_t(std::span<const std::byte>)>;

    /**
     * @brief Constructor.
     * @param storage Storage reference.
     * @param crc Function calculating CRC.
     */
    explicit safe(Storage& storage, crc_callback&& crc) : storage_{storage}, crc_{std::move(crc)} {}

    /**
     * @brief Initializes safe storage.
     * Method will try to load data from slot identified by status byte.
     * If slot is not valid, it will try to load data from other slot.
     * If both slots are invalid, safe will initialize data with default value.
     */
    void initialize() {
        value_ = T{};
        current_slot_ = 0;

        read_status().map([this](std::uint8_t status) {
            if (status <= 1) {
                if (read_slot(status)) {
                    current_slot_ = status;
                    return;
                }

                status = next_slot(status);

                if (read_slot(status)) {
                    current_slot_ = status;
                    return;
                }
            }

            save();
        });
    }

    /**
     * @brief Loads data from slot.
     * @return True if load was successful, false otherwise.
     */
    bool load() { return read_slot(current_slot_); }

    /**
     * @brief Saves current value to slot.
     * @return True if save was successful, false otherwise.
     */
    bool save() {
        using namespace satext::struct_literals;

        Serializer::serialize(value_, buffer_);

        if (auto crc = crc_(std::span(buffer_.data(), serialized_size));
            satext::pack_to("<I"_fmt, std::span(buffer_.data() + serialized_size, crc_size), crc) !=
            crc_size) {
            return false;
        }

        auto slot = next_slot(current_slot_);

        if (!storage_.write(slot_offset(slot), buffer_)) {
            return false;
        }

        if (write_status(slot)) {
            current_slot_ = slot;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @brief Returns current value.
     * @return current value.
     */
    T get() const { return value_; }

    /**
     * @brief Sets value and saves it to slot.
     * @param value Value.
     * @return True if save was successful, false otherwise.
     */
    bool set(const T& value) {
        value_ = value;
        return save();
    }

    /**
     * @brief Gets field from value using projection.
     * @tparam Proj Projection type.
     * @param proj Projection.
     * @return Field value.
     */
    template<typename Proj>
    auto get(Proj proj) const {
        return std::invoke(proj, value_);
    }

    /**
     * @brief Sets field in value using projection and saves it to slot.
     * @tparam Proj Projection type.
     * @tparam U Field type.
     * @param proj Projection.
     * @param value Field value.
     * @return True if save was successful, false otherwise.
     */
    template<typename Proj, typename U>
    bool set(Proj proj, U&& value) {
        std::invoke(proj, value_) = std::forward<U>(value);
        return save();
    }

    /**
     * @brief Returns current slot.
     * @return current slot.
     */
    std::uint8_t get_current_slot() const { return current_slot_; }

private:
    static constexpr auto crc_size = sizeof(std::uint32_t);
    static constexpr auto status_offset = 0u;
    static constexpr auto status_size = sizeof(std::uint8_t);
    static constexpr auto serialized_size = serialized_sizeof<T, Serializer>();
    static constexpr auto slot_size = serialized_size + crc_size;

    std::uint8_t next_slot(std::uint8_t slot) const { return (slot + 1) % 2; }

    std::size_t slot_offset(std::uint8_t slot) const { return status_size + slot * slot_size; }

    satext::expected<std::uint8_t, std::monostate> read_status() {
        std::byte status{};
        if (!storage_.read(status_offset, std::span(&status, 1))) {
            return satext::unexpected{std::monostate{}};
        } else {
            return static_cast<std::uint8_t>(status);
        }
    }

    bool write_status(std::uint8_t status) {
        std::byte data{status};
        return storage_.write(status_offset, std::span(&data, 1));
    }

    bool read_slot(std::uint8_t slot) {
        using namespace satext::struct_literals;
        if (!storage_.read(slot_offset(slot), buffer_)) {
            return false;
        }

        return satext::unpack("<I"_fmt, std::span(buffer_.data() + serialized_size, crc_size))
            .map([this](auto unpacked) {
                auto [crc] = unpacked;
                if (crc_(std::span(buffer_.data(), serialized_size)) == crc) {
                    Serializer::deserialize(buffer_, value_);
                    return true;
                } else {
                    return false;
                }
            })
            .value_or(false);
    }

    T value_{};
    std::array<std::byte, slot_size> buffer_{};
    Storage& storage_;
    crc_callback crc_;
    std::uint8_t current_slot_{};
};

/** @} */

} // namespace safe

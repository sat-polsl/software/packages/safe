#pragma once
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <span>

namespace safe {

// clang-format off
template<typename T>
concept storage = requires(T t,
                           std::size_t offset,
                           std::span<std::byte> output,
                           std::span<const std::byte> input) {
    { t.read(offset, output) } -> std::same_as<bool>;
    { t.write(offset, input) } -> std::same_as<bool>;
};

template <typename T, typename U>
concept serializer = requires(T t, U u, std::span<std::byte> output, std::span<const std::byte> input) {
    { T::serialize(u, output) } -> std::same_as<void>;
    { T::deserialize(input, u) } -> std::same_as<void>;
};
// clang-format on

} // namespace safe
